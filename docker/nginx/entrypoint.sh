#!/usr/bin/env sh

set -eu

envsubst '${PHP_UPSTREAM_HOST} ${PHP_UPSTREAM_PORT}' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf

exec "$@"
