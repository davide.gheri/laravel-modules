<?php

namespace App\Providers;

use App\Modules\Auth\AuthServiceProvider;
use App\Modules\Users\UsersServiceProvider;
use Illuminate\Support\AggregateServiceProvider;

class ModulesServiceProvider extends AggregateServiceProvider
{
    protected $providers = [
        AuthServiceProvider::class,
        UsersServiceProvider::class
    ];
}
