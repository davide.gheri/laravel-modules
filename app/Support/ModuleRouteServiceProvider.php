<?php

namespace App\Support;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

abstract class ModuleRouteServiceProvider extends ServiceProvider
{
    use ModuleRouteServiceTrait;
}
