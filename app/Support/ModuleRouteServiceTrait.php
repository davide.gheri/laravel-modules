<?php

namespace App\Support;

use Illuminate\Support\Facades\Route;

trait ModuleRouteServiceTrait
{
    protected abstract function moduleNameSpace(): string;

    protected $webFile = '/routes/web.php';

    protected $apiFile = '/routes/api.php';

    protected function getPath(string $path): string {
        $ref = new \ReflectionClass(get_class($this));
        return dirname($ref->getFileName()) . $path;
    }

    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();
    }

    protected function mapWebRoutes()
    {
        $webFile = $this->getPath($this->webFile);
        if (file_exists($webFile)) {
            Route::middleware('web')
                ->namespace($this->moduleNamespace())
                ->group($webFile);
        }
    }

    protected function mapApiRoutes()
    {
        $apiFile = $this->getPath($this->apiFile);
        if (file_exists($apiFile)) {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->moduleNamespace())
                ->group($apiFile);
        }
    }
}
