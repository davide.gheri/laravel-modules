<?php

namespace App\Support;

use Illuminate\Database\Eloquent\Factory;

trait ModuleServiceTrait {
    protected $configFile = '/config/config.php';

    protected $factoryDir = '/database/factories';

    protected $migrationDir = '/database/migrations';

    protected abstract function configName(): string ;

    protected abstract function register();

    protected function getPath(string $path): string {
        $ref = new \ReflectionClass(get_class($this));
        return dirname($ref->getFileName()) . $path;
    }

    public function boot()
    {
        $this->registerConfig();
        $this->registerFactories();
        $this->registerMigrations();
    }

    protected function registerConfig()
    {
        $configFile = $this->getPath($this->configFile);
        if (file_exists($configFile)) {
            $this->mergeConfigFrom(
                $configFile, $this->configName()
            );
        }
    }

    protected function registerFactories()
    {
        $factoryDir = $this->getPath($this->factoryDir);
        if (!$this->app->environment('production')
            && $this->app->runningInConsole()
            && file_exists($factoryDir)
        ) {
            $this->app->get(Factory::class)->load($factoryDir);
        }
    }

    protected function registerMigrations()
    {
        $migrationDir = $this->getPath($this->migrationDir);
        if (file_exists($migrationDir)) {
            $this->loadMigrationsFrom($migrationDir);
        }
    }
}
