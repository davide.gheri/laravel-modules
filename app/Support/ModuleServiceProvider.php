<?php

namespace App\Support;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;

abstract class ModuleServiceProvider extends ServiceProvider
{
    use ModuleServiceTrait;
}
