<?php

namespace App\Modules\Auth;

use App\Support\ModuleServiceTrait;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Str;
use Laravel\Passport\Client;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    use ModuleServiceTrait {
        boot as bootTrait;
        registerConfig as registerConfigTrait;
    }

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    protected function configName(): string
    {
        return 'auth';
    }

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootTrait();

        $this->registerPolicies();

        $this->defineGates();

        Passport::routes();

        Client::creating(function(Client $client) {
            $client->incrementing = false;
            $client->id = Str::uuid();
        });

        Client::retrieved(function(Client $client) {
            $client->incrementing = false;
        });
    }

    public function register()
    {
        Passport::ignoreMigrations();

        $this->app->register(RouteServiceProvider::class);
    }

    protected function registerConfig()
    {
        $this->registerConfigTrait();

        $oldPermissionConfig = $this->app['config']->get('permission', []);

        $this->app['config']->set('permission', array_merge($oldPermissionConfig, require __DIR__ . '/config/permission.php'));
    }

    protected function defineGates()
    {
        Gate::define('test', function($user) {
           return true;
        });
    }
}
