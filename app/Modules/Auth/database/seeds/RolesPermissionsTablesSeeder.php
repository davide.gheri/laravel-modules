<?php

namespace App\Modules\Auth\Database\Seeds;

use App\Modules\Auth\Models\Permission;
use App\Modules\Auth\Models\Role;
use App\Modules\Users\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\PermissionRegistrar;

class RolesPermissionsTablesSeeder extends Seeder
{
    protected $crud = ['create', 'read', 'update', 'delete'];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(
        Role $role,
        Permission $permission,
        User $user
    )
    {
        app(PermissionRegistrar::class)->forgetCachedPermissions();

        $this->seedUserPermissions($permission);

        $adminRole = $role->create(['name' => 'admin'])
            ->givePermissionTo($permission->all());
        $subscriberRole = $role->create(['name' => 'subscriber']);

        $adminUser = $user->where('email', 'admin@admin.com')->first();
        $adminUser->assignRole($adminRole);
    }

    protected function seedUserPermissions(Permission $permission)
    {
        foreach ($this->crud as $name) {
            $permission->create(['name' => $name . '-users']);
        }
    }
}
