<?php

namespace App\Modules\Auth;

use App\Support\ModuleRouteServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ModuleRouteServiceProvider
{
    protected function moduleNameSpace(): string
    {
        return 'App\Modules\Auth\Http\Controllers';
    }
}
