<?php

namespace App\Modules\Auth\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Users\Http\Resources\UsersResource;
use App\Modules\Users\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    public function me(Request $request)
    {
        return new UsersResource($request->user());
    }

    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);

        $user = $this->guard()->user();

        $token = $user->createToken(config('auth.token_name'))->accessToken;

        return response()->json(['token' => $token], 200);
    }
}
