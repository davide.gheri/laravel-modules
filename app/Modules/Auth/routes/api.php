<?php
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'auth',
], function() {
    Route::post('login', 'AuthController@login');

    Route::middleware('auth:api')->get('me', 'AuthController@me');
});
