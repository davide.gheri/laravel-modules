<?php

use Illuminate\Support\Facades\Route;

Route::apiResource('users', UsersController::class)->middleware('auth:api');
