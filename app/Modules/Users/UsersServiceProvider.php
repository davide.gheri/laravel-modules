<?php

namespace App\Modules\Users;

use App\Support\ModuleServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class UsersServiceProvider extends ModuleServiceProvider
{

    protected function configName(): string
    {
        return 'users';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
