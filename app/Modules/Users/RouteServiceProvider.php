<?php

namespace App\Modules\Users;

use App\Support\ModuleRouteServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ModuleRouteServiceProvider
{
    protected function moduleNameSpace(): string
    {
        return 'App\Modules\Users\Http\Controllers';
    }
}
