FROM php:fpm-alpine

RUN apk add \
    libmemcached-dev

RUN docker-php-ext-install pdo_mysql

ARG PHP_DEBUG=false

RUN if [ ${PHP_DEBUG} = true ]; then \
        mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini" \
    ;fi


WORKDIR /var/www

CMD ["php-fpm"]

EXPOSE 9000
