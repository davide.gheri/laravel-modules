<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(\App\Modules\Users\Database\Seeds\UsersTableSeeder::class);
         $this->call(\App\Modules\Auth\Database\Seeds\RolesPermissionsTablesSeeder::class);
    }
}
